import { Component } from '@angular/core';

@Component({
  selector: 'fjjk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'todo';
}
